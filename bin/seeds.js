require('dotenv').config()
const mongoose = require('mongoose')
const User = require('../models/user.model');
require('../config/db.config')

const dbtitle = 'informes';
mongoose.connect(`mongodb://localhost/${dbtitle}`);
const pasword = process.env.PASWORD_DBS

User.deleteMany()
.then(data => {
    console.log('ELIMINADOS' + JSON.stringify(data))

    const users = [
        {
            username: 'Julián de las Heras',
            email: 'juliandelasheras@impactoseo.com',
            password: pasword,
            phone: '689 42 34 88',
        },
        {
            username: 'María de las Heras',
            email: 'mariadelheras@impactoseo.com',
            password: pasword,
            phone: '676 36 39 71',
        },
        {
            username: 'Estela Fernández',
            email: 'estelafdz@impactoseo.com',
            password: pasword,
            phone: '91 109 05 02',
        }
    ]
    
    const createUsers = users.map(user => {
        console.log(user)
        const newUser = new User(user)
        return newUser.save()
            .then(user => {
                console.log(user)
            })
            .catch(error => {
                console.log(error)
                throw new Error(`Impossible to add the user. ${error}`)
            })
    })
    return Promise.all(createUsers)
    .then(
        data => {
            return data;
        }
    )
    .catch(error => {
        console.log(error)
        throw new Error(error)
    })
})
.catch(error => console.error(error))
.then(()=>process.exit(0))