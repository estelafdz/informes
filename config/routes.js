const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/auth.middleware')
const generalController = require('../controllers/general.controller')
const usersController = require('../controllers/users.controller')

module.exports = router;

router.get('/', authMiddleware.isAuthenticated, generalController.index)

//router.get('/users/create', authMiddleware.isAuthenticated, usersController.create)
//router.post('/users/create', authMiddleware.isAuthenticated, usersController.doCreate)

router.get('/login', usersController.login)
router.post('/login', usersController.doLogin)
router.get('/logout', authMiddleware.isAuthenticated, usersController.logout)

router.post('/form', authMiddleware.isAuthenticated, generalController.form)
router.get('/pdf', authMiddleware.isAuthenticated, generalController.getInforme)


