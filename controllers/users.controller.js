const createError = require('http-errors');
const mongoose = require('mongoose');
const User = require('../models/user.model');


// GET '/users/create' -> Enviamos al usuario a la vista 'newUser.hbs'
module.exports.create = (req, res, next) => {
    res.render('newUser', {user: new User() })
}

// POST '/users/create' -> Cuando el usuario rellene los datos y haga clic en enviar, creamos un nuevo usuario en la base de datos
module.exports.doCreate = (req, res, next) => {
    const user = new User ({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
      })
    
      user.save()
      .then((user) => {
        res.redirect('/')
      })
      .catch(error => {
        if (error instanceof mongoose.Error.ValidationError) {
          res.render('users/new', {user, error: error.errors })
        } else if (error.code === 1100) {
          res.render ('users/new', {
            user: {
              ...user,
              password: null
            },
            genericError: 'User exist'
          })
        } else {
          next(error);
        }
      })
}

// GET '/login' -> Enviamos al usuario a la vista 'login.hbs'
module.exports.login = (req, res, next) => {
    res.render('login')
}

// POST '/login' -> cuando el usuario haga clic en enter, revisamos que todos los datos sean correctos con los que tenemos en la base de datos. Si son correctos redirigimos a '/' (esto envia al usuario a la vista 'index.hbs') , si no son correctos redirigimos a '/login' 
module.exports.doLogin = (req, res, next) => {
    const email = req.body.email
    const password = req.body.password
  
    User.findOne({ email: email })
    .then(user => {
    if (!user) {
        req.session.genericError = 'Credenciales incorrectas'
        res.redirect('/login')
    }
    else {
        if(!user.checkPassword(password)) {
          req.session.genericError = 'Credenciales incorrectas'
          res.redirect('/login')
        } else {
          req.session.user = user
          res.redirect('/')
        }
      
      /*
      user.checkPassword(password)
        .then(match => {
            if (!match) {
            req.session.genericError = 'Credenciales incorrectas'
            res.redirect('/login')
            } else {
            req.session.user = user
            res.redirect('/')
            }
        })*/
      }
    })
    .catch(next)
}

module.exports.logout = (req, res, next) => {
    res.clearCookie('connect.sid')
    req.session.destroy();
    res.redirect('/');
}