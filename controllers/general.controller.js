const createError = require('http-errors');
const mongoose = require('mongoose');
const axios = require('axios');
const cheerio = require('cheerio');
const Informe = require('../models/informe.model');
const Respuesta = require('../models/respuesta.model');
const User = require('../models/user.model');
const pdf = require('html-pdf');
const fs = require('fs');
const path = require('path');
const hbs = require ('handlebars');



module.exports.index = (req, res, next) => {
    res.render('index')
    console.log(req.session.user)

}

module.exports.form = (req, res, next) => {

    const domain = req.body.url 
    const urlDom = domain.replace(/https:|\/|\//gi, '')

    //console.log(domain)
    //const domain = "https://impactoseo.com/"
    /*Informe.deleteMany()
    .then(deleted => {
        console.log(deleted)
    })
    .catch(error => {
        next(error)
    })*/

    Informe.findOneAndDelete({ 'url.name': urlDom})
    .then(element => {
        console.log(element)
        // Robots
        axios.get(`${domain}robots.txt`)
        .then(result => {
            let robots = "si"
            return robots
        })
        .catch(error => {
            robots = "no"
            return robots
        })
        .then(robots => {

            //Sitemaps
            axios.get(`${domain}sitemap.xml`)
            .then(result => {
                let sitemap = "si"
                return sitemap
            })
            .catch(error => {
                sitemap = "no"
                return sitemap
            })
            .then(sitemap => {

                axios.get(domain)
                .then(data =>{
                    
                    //const domain = 'https://madrid-asesoria.es/'
                    const html = data.data
                    const $ = cheerio.load(html)
                    
                    //URL
                    const urlNum = urlDom.length
                    
                    //Camonical
                    const canonical = $('link[rel="canonical"]').attr('href');
                    let canonicalResult = ''
                    if (canonical == domain || canonical == `${domain}/` ) {
                        canonicalResult = 'si'
                    } else {
                        canonicalResult = 'no'
                    }
                    
                    // Título
                    const title = $('title').first().text();
                    let titleResult = ''
                    if (title != undefined) {
                        titleResult = `${title}`
                    } else {
                        titleResult = ''
                    }

                    const titleNum = title.length
                    
                    const array = title.toLowerCase().replace(/,|:|-/gi, '').split(' ') 

                    array.forEach(e => {
                        if(e == 'la' || e == 'las' || e == 'a' || e == 'le' || e == 'les' || e == 'e' || e == 'y' || e == 'o' || e == 'lo' || e == 'los' || e == 'de' ) {
                            
                            array.splice(array.indexOf(e), 1)
                        }
                    })

                    let seRepiten = false
                    var iguales=0;
                    for(var i=0;i<array.length;i++)
                    {
                        for(var j=0;j<array.length;j++)
                        {
                            if(array[i]==array[j])
                                iguales++;
                            if(iguales >=2) {
                            seRepiten = true 
                            }
                        }
                        iguales = 0
                    }                        

                
                    // Metadescripción
                    const metadescripcion = $('meta[name="description"]').attr('content');
                    let metadescripcionResult = ''
                    if (metadescripcion != undefined) {
                        metadescripcionResult = `${metadescripcion}`
                    } else {
                        metadescripcionResult = ''
                    }

                    const metadescripcionNum = metadescripcionResult.length
                
                    // Palabras clave
                    const keywords = $('meta[name="keywords"]').attr('content');
                    let keywordResult = []
                    let keywordsChar = 0
                    if (keywords != undefined) {
                        keywordResult = keywords.split(' ')
                        keywordsChar = keywords.length
                    } else {
                        keywordResult = []
                    }

                    const newKeyWord = keywordResult.map(e => e.replace(',','<span class="point"> · <span>'))

                    const keywordsNum = keywordResult.length

                
                    // Encabezados
                    let h1 = [];
                    let h2 = [];
                    let h3 = [];
                    let h4 = [];
                    let h5 = [];
                    let h6 = [];
                
                    $('h1').each (function () {
                        if($(this).text().length !== 0) {
                            h1.push($(this).text())
                        }  
                    });
                
                    $('h2').each (function () {
                        if($(this).text().length !== 0) {
                            h2.push($(this).text())
                        }  
                    });
                
                    $('h3').each (function () {
                        if($(this).text().length !== 0) {
                            h3.push($(this).text())
                        }  
                    });
                
                    $('h4').each (function () {
                        if($(this).text().length !== 0) {
                            h4.push($(this).text())
                        }  
                    });
                
                    $('h5').each (function () {
                        if($(this).text().length !== 0) {
                            h5.push($(this).text())
                        }  
                    });    
                
                    $('h6').each (function () {
                        if($(this).text().length !== 0) {
                            h6.push($(this).text())
                        }  
                    });
                    
                    let jerarquia = false

                    if (h1.length < h2.length && h2.length < h3.length && h3.length < h4.length && h4.length < h5.length && h5.length < h6.length) {
                        jerarquia = true
                    }
                    
                    let nonexistent = false

                    if(h1.length == 0 && h2.length == 0 && h3.length == 0 && h4.length == 0 && h5.length == 0 && h6.length == 0) {
                        nonexistent = true
                    }
                    // Alts
                    let alt = '';
                    let altResult = [];
                    var noAlt = 0;
                    $('img').each (function () {
                        alt = $(this).attr('alt');
                        if (alt === undefined || alt == "") {
                            noAlt ++; 
                        } else {
                            altResult.push($(this).attr('alt'))
                        }
                    });
            
                    /*$('img[alt]').each (function (altRes) {
                        if (altRes !== "" || altRes !== '') {
                            altResult.push($(this).attr('alt'))
                        }
                    });*/
            
                    const totalAlt = altResult.length
                    const totalImg = $('img').length

                    const porcent = Math.round(totalImg/totalAlt)
                    
                    // Ratio Texto/HTML
                    const text = $('body').text().length;
                    const textScript = $('noscript').text().length;
                    const totalText = text - textScript;
                    const allHTML = $('body').html().length;
            
                    const ratio = Math.round((totalText / allHTML)*100)
                
                    // Marcos
                    const iframe = $('iframe');
                    let iframeResult = ''
            
                    if (iframe != undefined) {
                        iframeResult = "si"
                    } else {
                        iframeResult = "no"
                    } 
                
                    // Schema.org
                    const schema = html.indexOf('http://schema.org');
                    let schemaResult = ''
            
                    if (schema >= 0) {
                        schemaResult = "Tu página utiliza marcado Schema.org"
                    } else {
                        schemaResult = "Tu página NO utiliza marcado Schema.org."
                    }
                    
                    //Idioma
                
                    const idioma = $('html').attr('lang');
                    let idiomaResult = ''
                    let idiomaName = ''
            
                    if (idioma != undefined) {
                        idiomaResult = `${idioma}`
                        if(idiomaResult == 'es-ES' || idiomaResult == 'es' || idiomaResult == 'es-Es') {
                            idiomaName = 'Español'
                        } else if(idiomaResult == 'en-EN' || idiomaResult == 'en' || idiomaResult == 'en-En') {
                            idiomaName = 'Inglés/English'
                        } else {
                            idiomaName = idioma
                        }
                    } else {
                        idiomaResult = 'Indefinido'
                        idiomaName = 'Indefinido'
                    }
                
                    // Codificación
                    const codificacion = $('meta[charset]').attr('charset');
                    let codificacionResult = ''
            
                    if (codificacion != undefined) {
                        codificacionResult = codificacion
                    } else {
                        codificacionResult = "La coficicación no está definida en su sitio"
                    }
                
                    // Google Analytics
                    const analytics = html.indexOf('www.google-analytics.com/analytics.js');
                    let analyticsResult = ''
            
                    if (analytics >= 0) {
                        analyticsResult = "si"
                    } else {
                        analyticsResult = "no"   
                    }
                
                    // Favicon
                    const favicon = $('link[rel="icon"]').attr('href');
                    let faviconResult = ''
            
                    if (favicon != undefined) {
                        faviconResult = "si"
                    } else {
                        faviconResult = "no"
                    }
                    
                    
                    const informe = new Informe({
                        url: {
                            name: domain.replace(/https:|\/|\//gi, ''),
                            numCharacters: urlNum,
                        },
                        canonical: canonicalResult,
                        titulo: {
                            name: titleResult,
                            numCharacters: titleNum,
                            seRepiten: seRepiten,
                        },
                        metadescripcion: {
                            name: metadescripcionResult,
                            numCharacters: metadescripcionNum,
                        },
                        palabrasClave: { 
                            palabrasClave: newKeyWord,
                            numPC: keywordsNum,
                            numCharacters: keywordsChar,
                        },
                        encabezados: {
                            h1: {
                                names: h1,
                                number: h1.length,
                            },
                            h2: {
                                names: h2,
                                number: h2.length,
                            },
                            h3: {
                                names: h3,
                                number: h3.length,
                            },
                            h4: {
                                names: h4,
                                number: h4.length,
                            },
                            h5: {
                                names: h5,
                                number: h5.length,
                            },
                            h6: {
                                names: h6,
                                number: h6.length,
                            },
                            jerarquia: jerarquia,
                            nonexistent: nonexistent,
                        },
                        alts: {
                            names: altResult,
                            numImgs: totalImg,
                            numAlts: totalAlt,
                            noAlts: noAlt,
                            porcent: porcent,
                        },
                        ratio: ratio,
                        marcos: iframeResult,
                        esquema: schemaResult,
                        //openGraph: String,
                        //tarjetaTwitter: String,
                        robots: robots,
                        sitemaps: sitemap,
                        idioma: {
                            name: idiomaName,
                            type: idiomaResult,
                        },
                        //tipoDoc: String,
                        codificacion: codificacionResult,
                        analytics: analyticsResult,
                        favicon: faviconResult,
                    })
                    
                    informe.save()
                    .then((informe) => {
                        console.log(informe)
                        res.render('form', { informe })
                    })
                    .catch(error => {
                        console.log(error)
                    })
                })
            })
        })

    })

}

module.exports.getInforme = (req, res, next) => {
    
    //Guardamos en una variable nuestro actual usuario
    const user = req.session.user

    Informe.findOne({"url.name": req.query.domain})
    .then(informe => {

        const titulo = informe.titulo.name
        const proyecto = req.query.proyecto
        
        console.log(informe.encabezados.h4.number)

        Respuesta.findOneAndUpdate({domain: req.query.domain}, {
            url: req.query.url,
            canonical: req.query.canonical,
            titulo: req.query.titulo,
            proyecto: titulo.toLowerCase().includes(proyecto.toLowerCase()) ? true : false,
            metadescripcion: req.query.metadescripcion,
            palabrasClave: req.query.palabrasClave,
            encabezados: req.query.encabezados,
            alt: req.query.alt,
            ratio: req.query.ratio,
            marcos: req.query.marcos,
            esquema: req.query.esquema,
            robots: req.query.robots,
            sitemaps: req.query.sitemaps,
            idioma: req.query.idioma,
            codificacion: req.query.codificacion,
            analytics: req.query.analytics,
            favicon: req.query.favicon,
        }, 
        {
            new: true
        })
        .then(respuesta => {
            if(respuesta) {


                hbs.registerHelper('ifCond', function (v1, operator, v2, options) {

                    switch (operator) {
                        case '==':
                            return (v1 == v2) ? options.fn(this) : options.inverse(this);
                        case '===':
                            return (v1 === v2) ? options.fn(this) : options.inverse(this);
                        case '!=':
                            return (v1 != v2) ? options.fn(this) : options.inverse(this);
                        case '!==':
                            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                        case '<':
                            return (v1 < v2) ? options.fn(this) : options.inverse(this);
                        case '<=':
                            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                        case '>':
                            return (v1 > v2) ? options.fn(this) : options.inverse(this);
                        case '>=':
                            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                        case '&&':
                            return (v1 && v2) ? options.fn(this) : options.inverse(this);
                        case '||':
                            return (v1 || v2) ? options.fn(this) : options.inverse(this);
                        default:
                            return options.inverse(this);
                    }
                });

                hbs.registerHelper('limit', function (arr, limit) {
                if (!Array.isArray(arr)) { return []; }
                return arr.slice(0, limit);
                });
                
                User.findOne({ email: user.email})
                .then(user => {

                    console.log(user)

                    var templateHtml = fs.readFileSync(path.join(__dirname, '../views/informe.hbs'), 'utf8');
                    var template = hbs.compile(templateHtml);
                    var html = template({informe, respuesta, user});
                    
                    var options = {
                        "format": 'A3',
                        "footer": `<footer class="backBlue">
                        <div class="footer">
                            <div class="footer-row">
                                <div>
                                    <p>${user.username}</p>
                                    <img src="" alt="">
                                </div>
                                    <div>
                                    <p>${user.phone}</p>
                                    <img src="" alt="">
                                </div>
                                    <div>
                                    <p>${user.email}</p>
                                    <img src="" alt="">
                                </div>        
                            </div>    
                        </div>
                        </footer>`,
                    };

                    pdf.create(html, options).toFile(`./pdf/Informe-SEO-${informe.url.name}.pdf`, function(err, res) {
                        if (err){
                            console.log(err);
                        } else {
                            console.log(res);
                        }
                    });
    
                    console.log(informe.encabezados.h4.number)
                    res.render('informe', { informe, respuesta, user })
                })
            }
            else {
                const respuesta = new Respuesta ({
                    domain: req.query.domain,
                    url: req.query.url,
                    canonical: req.query.canonical,
                    titulo: req.query.titulo,
                    proyecto: titulo.toLowerCase().includes(proyecto.toLowerCase()) ? true : false,
                    metadescripcion: req.query.metadescripcion,
                    palabrasClave: req.query.palabrasClave,
                    encabezados: req.query.encabezados,
                    alt: req.query.alt,
                    ratio: req.query.ratio,
                    marcos: req.query.marcos,
                    esquema: req.query.esquema,
                    robots: req.query.robots,
                    sitemaps: req.query.sitemaps,
                    idioma: req.query.idioma,
                    codificacion: req.query.codificacion,
                    analytics: req.query.analytics,
                    favicon: req.query.favicon,
                })
            
                respuesta.save()
                .then((respuesta) => {
                    
                    User.findOne({ email: user.email})
                    .then(user => {

                        console.log(user)

                        var templateHtml = fs.readFileSync(path.join(__dirname, '../views/informe.hbs'), 'utf8');
                        var template = hbs.compile(templateHtml);
                        var html = template({informe, respuesta});
                        
                        var options = {
                            "format": 'A3',
                            "footer": `<footer class="backBlue">
                            <div class="footer">
                                <div class="footer-row">
                                    <div>
                                        <p>${user.username}</p>
                                        <img src="" alt="">
                                    </div>
                                        <div>
                                        <p>${user.phone}</p>
                                        <img src="" alt="">
                                    </div>
                                        <div>
                                        <p>${user.email}</p>
                                        <img src="" alt="">
                                    </div>        
                                </div>    
                            </div>
                            </footer>`,
                        };
                        
                        pdf.create(html, options).toFile(`./pdf/Informe-SEO-${informe.url.name}.pdf`, function(err, res) {
                            if (err){
                                console.log(err);
                            } else {
                                console.log(res);
                            }
                        });
    
                        console.log(informe.encabezados.h4.number)
                        res.render('informe', { informe, respuesta, user })
                    })
                    .catch(error => {
                        next(error)
                    })
                })
                .catch(error => {
                    next(error)
                })
            }
        })
    })
}

