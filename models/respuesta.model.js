const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const respuestaSchema = new Schema({
    domain: String,
    url: String,
    canonical: String,
    titulo: String,
    proyecto: Boolean,
    metadescripcion: String,
    palabrasClave: String,
    encabezados: String,
    alt: String,
    ratio: String,
    marcos: String,
    esquema: String,
    openGraph: String,
    tarjetaTwitter: String,
    robots: String,
    sitemaps: String,
    idioma: String,
    tipoDoc: String,
    codificacion: String,
    analytics: String,
    favicon: String,
})

const Respuesta = mongoose.model('Respuesta', respuestaSchema);
  
module.exports = Respuesta;