const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const informeSchema = new Schema({
    url: { 
      name: {type: String },
      numCharacters: {type: Number },
    },
    canonical: { type: String },
    titulo: { 
      name: {type: String },
      numCharacters: {type: Number },
      seRepiten: { type: Boolean },
    },
    metadescripcion: { 
      name: {type: String },
      numCharacters: {type: Number },
    },
    palabrasClave: { 
      palabrasClave: [{type: String }],
      numPC: {type: Number },
      numCharacters: {type: Number },
    },
    encabezados: {
      h1: {
        names: [{ type: String }],
        number: { type: Number },
      },
      h2: {
        names: [{ type: String }],
        number: { type: Number },
      },
      h3: {
        names: [{ type: String }],
        number: { type: Number },
      },
      h4: {
        names: [{ type: String }],
        number: { type: Number },
      },
      h5: {
        names: [{ type: String }],
        number: { type: Number },
      },
      h6: {
        names: [{ type: String }],
        number: { type: Number },
      },
      jerarquia: { type: Boolean },
      nonexistent: { type: Boolean },
    },
    alts: {
      names: [{ type: String }],
      numImgs: { type: Number },
      numAlts: { type: Number },
      noAlts: { type: Number },
      porcent: { type: Number },
    },
    ratio: { type: Number },
    marcos: { type: String  },
    esquema: { type: String  },
    openGraph: { type: String  },
    tarjetaTwitter: { type: String  },
    robots: { type: String  },
    sitemaps: { type: String  },
    idioma: {
      name: { type: String },
      type: { type: String },
    },
    tipoDoc: { type: String  },
    codificacion: { type: String  },
    analytics: { type: String  },
    favicon: { type: String  },
})

const Informe = mongoose.model('Informe', informeSchema);
  
module.exports = Informe;