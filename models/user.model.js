const mongoose = require('mongoose');
//const bcrypt = require('bcrypt-nodejs');
const EMAIL_PATTERN = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
//const Salt = process.env.SALT_WORK_FACTOR

const generateRandomToken = () => {
  return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
}

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Nombre requerido'],
    minlength: [3, 'El nombre requiere al menos 4 caracteres'],
    trim: true
  },
  email: {
    type: String,
    required: [true, 'Email requerido'],
    unique: true,
    trim: true,
    lowercase: true,
    match: [EMAIL_PATTERN, 'Email inválido']
  },
  password: {
    type: String,
    required: [true, 'Contraseña requerida'],
    minlength: [8, 'La contraseña requiere al menos 8 caracteres']
  },
  phone: {
    type: String,
    required: [true, 'Teléfono requerido'],
  },
}, { timestamps: true })

userSchema.methods.checkPassword = function (password) {
  return password === this.password
}

const User = mongoose.model('User', userSchema);

module.exports = User;