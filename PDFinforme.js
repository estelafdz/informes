const fs = require("fs");
const path = require("path");
const puppeteer = require('puppeteer');
const handlebars = require("handlebars");
const request = require('request');
const cheerio = require('cheerio');
const pdf = require('html-pdf');


hbs.registerHelper('ifCond', function (v1, operator, v2, options) {

	switch (operator) {
		case '==':
			return (v1 == v2) ? options.fn(this) : options.inverse(this);
		case '===':
			return (v1 === v2) ? options.fn(this) : options.inverse(this);
		case '!=':
			return (v1 != v2) ? options.fn(this) : options.inverse(this);
		case '!==':
			return (v1 !== v2) ? options.fn(this) : options.inverse(this);
		case '<':
			return (v1 < v2) ? options.fn(this) : options.inverse(this);
		case '<=':
			return (v1 <= v2) ? options.fn(this) : options.inverse(this);
		case '>':
			return (v1 > v2) ? options.fn(this) : options.inverse(this);
		case '>=':
			return (v1 >= v2) ? options.fn(this) : options.inverse(this);
		case '&&':
			return (v1 && v2) ? options.fn(this) : options.inverse(this);
		case '||':
			return (v1 || v2) ? options.fn(this) : options.inverse(this);
		default:
			return options.inverse(this);
	}
  });

const informe = {
    url: { 
		name: 'urlName',
		numCharacters: '24',
	},
	canonical: 'canonicalName',
	titulo: { 
		name: 'tituloName',
		numCharacters: '28',
		seRepiten: true,
	},
	metadescripcion: { 
		name: 'nameName',
		numCharacters: '4',
	},
}

const respuesta = {
	domain: 'domain',
	url: 'url',
	canonical: 'canonical',
	titulo: 'titulo',
	proyecto: false,
}


var templateHtml = fs.readFileSync(path.join(__dirname, '/views/pdf.hbs'), 'utf8');
let template = handlebars.compile(templateHtml);
var html = template({informe, respuesta});
console.log(html);






/*
const data = {
    name: 'Marina',
    email: 'marina@gmail.com'
}

const data2 = {
	respuesta: 'respuesta',
	funciona: 'si',
}
var templateHtml = fs.readFileSync(path.join(__dirname, '/views/pdf.hbs'), 'utf8');
var template = handlebars.compile(templateHtml);
var html = template({data, data2});

console.log(html)

var options = {
    "format": 'A4',
    "border": "2cm",
    "header": {
        "height": "30px"
    },
    "footer": {
        "height": "22mm"
    },
};


pdf.create(html, options).toFile('./pdf/salida.pdf', function(err, res) {
    if (err){
        console.log(err);
    } else {
        console.log(res);
    }
});*/


/*
async function createPDF(data){

	var templateHtml = fs.readFileSync(path.join(__dirname, '/views/pdf.hbs'), 'utf8');
	var template = handlebars.compile(templateHtml);
	var html = template(data);

	var milis = new Date();
	milis = milis.getTime();

	var pdfPath = path.join('pdf', `Informe.pdf`);

	var options = {
		width: '1230px',
		displayHeaderFooter: false,
		margin: {
			top: "10px",
			bottom: "30px"
		},
		printBackground: true,
		path: pdfPath
	}

	const browser = await puppeteer.launch({
		args: ['--no-sandbox'],
		headless: true
	});

	var page = await browser.newPage();
	
	await page.goto(`data:text/html;charset=UTF-8,${html}`, {
		waitUntil: 'networkidle0'
	});

	await page.pdf(data);
	await browser.close();
}*/